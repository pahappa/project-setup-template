/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pahappa.systems.core.dao;

import org.pahappa.systems.models.HelloWorld;
import org.sers.webutils.server.core.dao.BaseDao;

/**
 *
 * @author Ray Gdhrt
 */
public interface HelloWorldDao extends BaseDao<HelloWorld>{
    
}
